package sample;

public class OddEven {

    public String addEvenNo(int a){
        String result;
        if(a % 2 == 0){
            result = "Even";
        } else {
            result = "odd";
        }

        return result;
    }
}
