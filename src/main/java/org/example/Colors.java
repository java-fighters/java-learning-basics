package org.example;

public class Colors {
    public void colorNames() {
        String[] colors = {"RED", "WHITE", "BLACK"};

        for (String s : colors) {
            System.out.println(s);
        }
    }

    public void colorPrice() {
        int[] colorPrices = {200, 300, 400};
        for (int i : colorPrices) {
            System.out.println(i);
        }
    }
}
